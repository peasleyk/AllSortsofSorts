#ifndef SORT_H
#define SORT_H
/*
 * Sort.h
 * Name: FILL ME IN
 *
 * November 2015
 */

#include <vector>
#include <string.h>
#include <iostream>
#include <algorithm>

#include "MaxHeap.h"


using namespace std;

// Selection, Bubble and Merge don't follow style guidelines because they annoy
// me. I'll do that when I have everything finished.
void selectionSort(vector<int>& data){
  int lowest; // The index of the lowest value found
  for(unsigned int i = 0; i < data.size(); i++){
    lowest = i;
    for(unsigned int j = i; j < data.size(); j++){
      if(data.at(j) < data.at(lowest)){
        lowest = j;
      }
    }
    if(data.at(lowest) < data.at(i)){
      int temp = data.at(lowest);
      data.at(lowest) = data.at(i);
      data.at(i) = temp;
    }
  }
}

void bubbleSort(vector<int>& data){
  for(unsigned int i = 0; i < data.size(); i++){
    for(unsigned int j = 0; j < data.size() - i; j++){
      if(j + 1 < data.size()){ // If j isn't the last index
        if(data.at(j) > data.at(j + 1)){
          int temp = data.at(j + 1);
          data.at(j + 1) = data.at(j);
          data.at(j) = temp;
        }
      }
    }
  }
}

// Merges everything together after we have broken it all up
void merge(vector<int>& data, int start, int middle, int end){
  // We will be reusing these; right now they are for loop counters
  int i = 0, j = 0;

  int length_left = middle - start + 1;
  int length_right = end - middle;

  // Temporary vectors
  vector<int> left(length_left);
  vector<int> right(length_right);

  for(int i = 0; i < length_left; i++){
    left[i] = data[start + i];
  }

  for(int j = 0; j < length_right; j++){
    right[j] = data[middle + j + 1];
  }

  // Reusing; now they are index counters...
  i = 0; // ... for the left vector
  j = 0; // ... for the right vector
  int k = start; // Index counter for data vector

  // Compare left and right and drop in values accordingly.
  while(i < length_left && j < length_right){
    if(left[i] <= right[j]){ // If the left element is the lesser
      data[k] = left[i];
      i++;
    }else{ // If the right element is the lesser
      data[k] = right[j];
      j++;
    }
    k++;
  }

  // If left and right were unevenly pulled from, we still have elements that
  // haven't been swapped. So we will repeat individually.
  while(i < length_left){
    data[k] = left[i];
    i++;
    k++;
  }

  while(j < length_right){
    data[k] = right[j];
    j++;
    k++;
  }
}

// Keeps on calling itself, making halves, until there is 1 element.
void mergeSortRecursive(vector<int>& data, int start, int end){
  // When there is one element, start will equal end
  if(start < end){
    int middle =(start + end) / 2;
    mergeSortRecursive(data, start, middle); // Recursion on left half
    mergeSortRecursive(data, middle + 1, end); // Recursion on right half
    merge(data, start, middle, end); // Merge it all together
  }
}

void mergeSort(vector<int>& data){
  mergeSortRecursive(data, 0, data.size() - 1);
}

// Merges everything together after we have broken it all up
void merge2(vector<int>& data, int start, int middle, int end){
  // We will be reusing these; right now they are for loop counters
  int i = 0, j = 0;

  int length_left = middle - start + 1;
  int length_right = end - middle;

  // Abort merge, go to n^2 sort
  if((unsigned) length_left >= data.size() / 2 ||
     (unsigned) length_right >= data.size() / 2){
    return;
  }

  // Temporary vectors
  vector<int> left(length_left);
  vector<int> right(length_right);

  for(int i = 0; i < length_left; i++){
    left[i] = data[start + i];
  }

  for(int j = 0; j < length_right; j++){
    right[j] = data[middle + j + 1];
  }

  // Reusing; now they are index counters...
  i = 0; // ... for the left vector
  j = 0; // ... for the right vector
  int k = start; // Index counter for data vector

  // Compare left and right and drop in values accordingly.
  while(i < length_left && j < length_right){
    if(left[i] <= right[j]){ // If the left element is the lesser
      data[k] = left[i];
      i++;
    }else{ // If the right element is the lesser
      data[k] = right[j];
      j++;
    }
    k++;
  }

  // If left and right were unevenly pulled from, we still have elements that
  // haven't been swapped. So we will repeat individually.
  while(i < length_left){
    data[k] = left[i];
    i++;
    k++;
  }

  while(j < length_right){
    data[k] = right[j];
    j++;
    k++;
  }
}

// Keeps on calling itself, making halves, until there is 1 element.
void mergeSortRecursive2(vector<int>& data, int start, int end){
  // When there is one element, start will equal end
  if(start < end){
    int middle =(start + end) / 2;
    mergeSortRecursive2(data, start, middle); // Recursion on left half
    mergeSortRecursive2(data, middle + 1, end); // Recursion on right half
    merge2(data, start, middle, end); // Merge it all together
  }
}

void mergeSort2(vector<int>& data){
  mergeSortRecursive2(data, 0, data.size() - 1);
  selectionSort(data);
}

int reposition(vector<int>& data, int left, int right, int right_spot){
  for(int i=left; i<right; ++i){
    if(data[i] <= right_spot){
      swap(data[i], data[left]);
      left ++;
    }
  }

  return left - 1;
}

void quicksort_2(vector<int>& data, int left, int right){
  if(left >= right) return;

  int pivot = left +(right - left) / 2;
  //Swapping the pivot variable and 0, or the middle+1 if the right side
  swap(data[pivot], data[left]);
  //data[left] is not in the right spot
  int midpoint = reposition(data, left + 1, right, data[left]);
  //swapping
  swap(data[left], data[midpoint]);

  quicksort_2(data, left, midpoint);
  quicksort_2(data, midpoint + 1, right);
}

void quickSort(vector<int>& data){
  quicksort_2(data,0,data.size());
}

void heapSort(vector<int>& data){
  MaxHeap<int> heap;
  if(data.size() == 0 || data.size() == 1){
    return;
  }

  for(unsigned int i  = 0; i < data.size(); i ++){
    heap.push(data.at(i));
  }
  data.clear(); //might be a better way
  while(heap.size() > 0){
    data.insert(data.begin(), heap.top());
    heap.pop();
  }
}

void nonCompareSort(vector<int>& data){
  int max_num = 0;
  vector<int> out;

  if(data.size() == 0 || data.size() == 1){
    return;
  }

  for(unsigned int i = 0; i < data.size(); i++){
    if(data[i] > max_num){
      max_num = data[i];
    }
  }

  int data_2[max_num + 1];
  for(int i = 0; i < max_num + 1; i++){
    data_2[i] = 0;
  }


  for(unsigned int i = 0; i < data.size(); i++){
    data_2[data[i]] += 1;
  }

  //keeps
  for(int i = 0; i < max_num + 1; i++){
    for(int j = 0; j < data_2[i]; j++){
      out.push_back(i);
    }
  }
  data = out;
}



#endif
