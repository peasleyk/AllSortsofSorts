#ifndef MAXHEAP_H
#define MAXHEAP_H
/*    file MaxHeap.h
      author Kyle Peasley
      date 11/18/15

      @description Implements a templated maximum queue, using a
      heap structure.
*/
#include <vector>
#include <iostream>
#include <stdexcept>
#include <algorithm>

using namespace std;

template <class T>
class MaxHeap {
public:

MaxHeap(){
  last_added = 0;
}

void push(T value){
  elements.push_back(value);
  bubbleUp(last_added);
  last_added++; //Added after Bubble up as If we increment, Bubbleup thinks we have a non existent element
}

void pop(){
  if(last_added == 0){
    return;
  }
  /*Swaps the lower most node with the highest, then deletes the last one
  	The root (least) is continually swapped to the bottom
  */

  swap(elements[0],elements[last_added-1]);
  elements.pop_back();
  last_added--;
  bubbleDown(0);
}

T top(){
  if(!elements.empty()){
    return elements[0];
  }else{
    throw logic_error("The queue is empty");
  }
}

unsigned int size(){
  return elements.size();
}

private:
int last_added;
vector<T> elements;

void bubbleUp(int i){
	/*Continually checks if the inserted node is larger than its parent
		if the node is larger than its parent, it gets swapped with its parent
	*/
  int parent_node = parent(i);
  while(i > 0 && elements[i] > elements[parent_node]){
    swap(elements[i],elements[parent_node]);
    i = parent_node;
    parent_node = parent(i);
  }
}

void bubbleDown(int curr_node){
  /*shamefully from the book
    The bubble down function checks first if the right node pos is less than
    current node pos, and of the right element is more than the current node
    if it is, then the program checks which is greater, the left node or right.
    The function then swaps.
    Else, the function  checks if the let node is greater than the current node, and switches them
    if the current node is greater than both the right and the left, to_swap never changes
    and nothing gets swapped, leaving curr_node at 0
   */
  do {
    int to_swap = -1; // if it is greater than 1, then it swaps. -1 is outside of the vector range
    int right_node = right(curr_node);

    if(right_node < last_added && elements[right_node] > elements[curr_node]){
      int left_node = left(curr_node);
      if(elements[left_node] > elements[right_node]){
        to_swap = left_node;
      }else{
        to_swap = right_node;
      }
    }else{
      int left_node = left(curr_node);
      if(left_node < last_added && elements[left_node] > elements[curr_node]){
        to_swap = left_node;
      }
    }
    if(to_swap >= 0){
      swap(elements[curr_node],elements[to_swap]);
    }
    curr_node = to_swap;
  } while(curr_node >= 0);
}

unsigned int left(int i){
  return 2 * i + 1;
}

unsigned  int right(int i){
  return 2 * i + 2;
}

unsigned int parent(int i ){
  return (i-1) / 2;
}

};

#endif
