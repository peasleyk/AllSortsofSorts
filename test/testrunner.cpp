/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#define _CXXTEST_HAVE_STD
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/ErrorPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::ErrorPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "cxxtest";
    status = CxxTest::Main< CxxTest::ErrorPrinter >( tmp, argc, argv );
    return status;
}
bool suite_SortTest_init = false;
#include "MySort_test.h"

static SortTest suite_SortTest;

static CxxTest::List Tests_SortTest = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_SortTest( "MySort_test.h", 13, "SortTest", suite_SortTest, Tests_SortTest );

static class TestDescription_suite_SortTest_testSelectionIsSortedEmpty : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testSelectionIsSortedEmpty() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 39, "testSelectionIsSortedEmpty" ) {}
 void runTest() { suite_SortTest.testSelectionIsSortedEmpty(); }
} testDescription_suite_SortTest_testSelectionIsSortedEmpty;

static class TestDescription_suite_SortTest_testSelectionIsSorted1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testSelectionIsSorted1() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 46, "testSelectionIsSorted1" ) {}
 void runTest() { suite_SortTest.testSelectionIsSorted1(); }
} testDescription_suite_SortTest_testSelectionIsSorted1;

static class TestDescription_suite_SortTest_testSelectionIsSorted2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testSelectionIsSorted2() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 53, "testSelectionIsSorted2" ) {}
 void runTest() { suite_SortTest.testSelectionIsSorted2(); }
} testDescription_suite_SortTest_testSelectionIsSorted2;

static class TestDescription_suite_SortTest_testSelectionIsSorted3 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testSelectionIsSorted3() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 60, "testSelectionIsSorted3" ) {}
 void runTest() { suite_SortTest.testSelectionIsSorted3(); }
} testDescription_suite_SortTest_testSelectionIsSorted3;

static class TestDescription_suite_SortTest_testSelectionIsSorted4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testSelectionIsSorted4() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 67, "testSelectionIsSorted4" ) {}
 void runTest() { suite_SortTest.testSelectionIsSorted4(); }
} testDescription_suite_SortTest_testSelectionIsSorted4;

static class TestDescription_suite_SortTest_testSelectionIsSorted5 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testSelectionIsSorted5() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 74, "testSelectionIsSorted5" ) {}
 void runTest() { suite_SortTest.testSelectionIsSorted5(); }
} testDescription_suite_SortTest_testSelectionIsSorted5;

static class TestDescription_suite_SortTest_testSelectionIsSorted6 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testSelectionIsSorted6() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 81, "testSelectionIsSorted6" ) {}
 void runTest() { suite_SortTest.testSelectionIsSorted6(); }
} testDescription_suite_SortTest_testSelectionIsSorted6;

static class TestDescription_suite_SortTest_testSelectionIsSorted7 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testSelectionIsSorted7() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 88, "testSelectionIsSorted7" ) {}
 void runTest() { suite_SortTest.testSelectionIsSorted7(); }
} testDescription_suite_SortTest_testSelectionIsSorted7;

static class TestDescription_suite_SortTest_testSelectionIsSorted8 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testSelectionIsSorted8() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 95, "testSelectionIsSorted8" ) {}
 void runTest() { suite_SortTest.testSelectionIsSorted8(); }
} testDescription_suite_SortTest_testSelectionIsSorted8;

static class TestDescription_suite_SortTest_testSelectionIsSorted9 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testSelectionIsSorted9() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 102, "testSelectionIsSorted9" ) {}
 void runTest() { suite_SortTest.testSelectionIsSorted9(); }
} testDescription_suite_SortTest_testSelectionIsSorted9;

static class TestDescription_suite_SortTest_testBubbleIsSortedEmpty : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testBubbleIsSortedEmpty() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 110, "testBubbleIsSortedEmpty" ) {}
 void runTest() { suite_SortTest.testBubbleIsSortedEmpty(); }
} testDescription_suite_SortTest_testBubbleIsSortedEmpty;

static class TestDescription_suite_SortTest_testBubbleIsSorted1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testBubbleIsSorted1() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 117, "testBubbleIsSorted1" ) {}
 void runTest() { suite_SortTest.testBubbleIsSorted1(); }
} testDescription_suite_SortTest_testBubbleIsSorted1;

static class TestDescription_suite_SortTest_testBubbleIsSorted2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testBubbleIsSorted2() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 124, "testBubbleIsSorted2" ) {}
 void runTest() { suite_SortTest.testBubbleIsSorted2(); }
} testDescription_suite_SortTest_testBubbleIsSorted2;

static class TestDescription_suite_SortTest_testBubbleIsSorted3 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testBubbleIsSorted3() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 131, "testBubbleIsSorted3" ) {}
 void runTest() { suite_SortTest.testBubbleIsSorted3(); }
} testDescription_suite_SortTest_testBubbleIsSorted3;

static class TestDescription_suite_SortTest_testBubbleIsSorted4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testBubbleIsSorted4() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 138, "testBubbleIsSorted4" ) {}
 void runTest() { suite_SortTest.testBubbleIsSorted4(); }
} testDescription_suite_SortTest_testBubbleIsSorted4;

static class TestDescription_suite_SortTest_testBubbleIsSorted5 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testBubbleIsSorted5() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 145, "testBubbleIsSorted5" ) {}
 void runTest() { suite_SortTest.testBubbleIsSorted5(); }
} testDescription_suite_SortTest_testBubbleIsSorted5;

static class TestDescription_suite_SortTest_testBubbleIsSorted6 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testBubbleIsSorted6() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 152, "testBubbleIsSorted6" ) {}
 void runTest() { suite_SortTest.testBubbleIsSorted6(); }
} testDescription_suite_SortTest_testBubbleIsSorted6;

static class TestDescription_suite_SortTest_testBubbleIsSorted7 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testBubbleIsSorted7() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 159, "testBubbleIsSorted7" ) {}
 void runTest() { suite_SortTest.testBubbleIsSorted7(); }
} testDescription_suite_SortTest_testBubbleIsSorted7;

static class TestDescription_suite_SortTest_testBubbleIsSorted8 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testBubbleIsSorted8() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 166, "testBubbleIsSorted8" ) {}
 void runTest() { suite_SortTest.testBubbleIsSorted8(); }
} testDescription_suite_SortTest_testBubbleIsSorted8;

static class TestDescription_suite_SortTest_testBubbleIsSorted9 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testBubbleIsSorted9() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 173, "testBubbleIsSorted9" ) {}
 void runTest() { suite_SortTest.testBubbleIsSorted9(); }
} testDescription_suite_SortTest_testBubbleIsSorted9;

static class TestDescription_suite_SortTest_testMergeIsSortedEmpty : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeIsSortedEmpty() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 181, "testMergeIsSortedEmpty" ) {}
 void runTest() { suite_SortTest.testMergeIsSortedEmpty(); }
} testDescription_suite_SortTest_testMergeIsSortedEmpty;

static class TestDescription_suite_SortTest_testMergeIsSorted1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeIsSorted1() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 188, "testMergeIsSorted1" ) {}
 void runTest() { suite_SortTest.testMergeIsSorted1(); }
} testDescription_suite_SortTest_testMergeIsSorted1;

static class TestDescription_suite_SortTest_testMergeIsSorted2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeIsSorted2() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 195, "testMergeIsSorted2" ) {}
 void runTest() { suite_SortTest.testMergeIsSorted2(); }
} testDescription_suite_SortTest_testMergeIsSorted2;

static class TestDescription_suite_SortTest_testMergeIsSorted3 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeIsSorted3() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 202, "testMergeIsSorted3" ) {}
 void runTest() { suite_SortTest.testMergeIsSorted3(); }
} testDescription_suite_SortTest_testMergeIsSorted3;

static class TestDescription_suite_SortTest_testMergeIsSorted4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeIsSorted4() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 209, "testMergeIsSorted4" ) {}
 void runTest() { suite_SortTest.testMergeIsSorted4(); }
} testDescription_suite_SortTest_testMergeIsSorted4;

static class TestDescription_suite_SortTest_testMergeIsSorted5 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeIsSorted5() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 216, "testMergeIsSorted5" ) {}
 void runTest() { suite_SortTest.testMergeIsSorted5(); }
} testDescription_suite_SortTest_testMergeIsSorted5;

static class TestDescription_suite_SortTest_testMergeIsSorted6 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeIsSorted6() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 223, "testMergeIsSorted6" ) {}
 void runTest() { suite_SortTest.testMergeIsSorted6(); }
} testDescription_suite_SortTest_testMergeIsSorted6;

static class TestDescription_suite_SortTest_testMergeIsSorted7 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeIsSorted7() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 230, "testMergeIsSorted7" ) {}
 void runTest() { suite_SortTest.testMergeIsSorted7(); }
} testDescription_suite_SortTest_testMergeIsSorted7;

static class TestDescription_suite_SortTest_testMergeIsSorted8 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeIsSorted8() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 237, "testMergeIsSorted8" ) {}
 void runTest() { suite_SortTest.testMergeIsSorted8(); }
} testDescription_suite_SortTest_testMergeIsSorted8;

static class TestDescription_suite_SortTest_testMergeIsSorted9 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeIsSorted9() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 244, "testMergeIsSorted9" ) {}
 void runTest() { suite_SortTest.testMergeIsSorted9(); }
} testDescription_suite_SortTest_testMergeIsSorted9;

static class TestDescription_suite_SortTest_testMergeThresholdIsSortedEmpty : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeThresholdIsSortedEmpty() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 252, "testMergeThresholdIsSortedEmpty" ) {}
 void runTest() { suite_SortTest.testMergeThresholdIsSortedEmpty(); }
} testDescription_suite_SortTest_testMergeThresholdIsSortedEmpty;

static class TestDescription_suite_SortTest_testMergeThresholdIsSorted1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeThresholdIsSorted1() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 259, "testMergeThresholdIsSorted1" ) {}
 void runTest() { suite_SortTest.testMergeThresholdIsSorted1(); }
} testDescription_suite_SortTest_testMergeThresholdIsSorted1;

static class TestDescription_suite_SortTest_testMergeThresholdIsSorted2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeThresholdIsSorted2() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 266, "testMergeThresholdIsSorted2" ) {}
 void runTest() { suite_SortTest.testMergeThresholdIsSorted2(); }
} testDescription_suite_SortTest_testMergeThresholdIsSorted2;

static class TestDescription_suite_SortTest_testMergeThresholdIsSorted3 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeThresholdIsSorted3() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 273, "testMergeThresholdIsSorted3" ) {}
 void runTest() { suite_SortTest.testMergeThresholdIsSorted3(); }
} testDescription_suite_SortTest_testMergeThresholdIsSorted3;

static class TestDescription_suite_SortTest_testMergeThresholdIsSorted4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeThresholdIsSorted4() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 280, "testMergeThresholdIsSorted4" ) {}
 void runTest() { suite_SortTest.testMergeThresholdIsSorted4(); }
} testDescription_suite_SortTest_testMergeThresholdIsSorted4;

static class TestDescription_suite_SortTest_testMergeThresholdIsSorted5 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeThresholdIsSorted5() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 287, "testMergeThresholdIsSorted5" ) {}
 void runTest() { suite_SortTest.testMergeThresholdIsSorted5(); }
} testDescription_suite_SortTest_testMergeThresholdIsSorted5;

static class TestDescription_suite_SortTest_testMergeThresholdIsSorted6 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeThresholdIsSorted6() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 294, "testMergeThresholdIsSorted6" ) {}
 void runTest() { suite_SortTest.testMergeThresholdIsSorted6(); }
} testDescription_suite_SortTest_testMergeThresholdIsSorted6;

static class TestDescription_suite_SortTest_testMergeThresholdIsSorted7 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeThresholdIsSorted7() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 301, "testMergeThresholdIsSorted7" ) {}
 void runTest() { suite_SortTest.testMergeThresholdIsSorted7(); }
} testDescription_suite_SortTest_testMergeThresholdIsSorted7;

static class TestDescription_suite_SortTest_testMergeThresholdIsSorted8 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeThresholdIsSorted8() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 308, "testMergeThresholdIsSorted8" ) {}
 void runTest() { suite_SortTest.testMergeThresholdIsSorted8(); }
} testDescription_suite_SortTest_testMergeThresholdIsSorted8;

static class TestDescription_suite_SortTest_testMergeThresholdIsSorted9 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testMergeThresholdIsSorted9() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 315, "testMergeThresholdIsSorted9" ) {}
 void runTest() { suite_SortTest.testMergeThresholdIsSorted9(); }
} testDescription_suite_SortTest_testMergeThresholdIsSorted9;

static class TestDescription_suite_SortTest_testQuickIsSortedEmpty : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testQuickIsSortedEmpty() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 323, "testQuickIsSortedEmpty" ) {}
 void runTest() { suite_SortTest.testQuickIsSortedEmpty(); }
} testDescription_suite_SortTest_testQuickIsSortedEmpty;

static class TestDescription_suite_SortTest_testQuickIsSorted1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testQuickIsSorted1() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 330, "testQuickIsSorted1" ) {}
 void runTest() { suite_SortTest.testQuickIsSorted1(); }
} testDescription_suite_SortTest_testQuickIsSorted1;

static class TestDescription_suite_SortTest_testQuickIsSorted2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testQuickIsSorted2() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 337, "testQuickIsSorted2" ) {}
 void runTest() { suite_SortTest.testQuickIsSorted2(); }
} testDescription_suite_SortTest_testQuickIsSorted2;

static class TestDescription_suite_SortTest_testQuickIsSorted3 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testQuickIsSorted3() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 344, "testQuickIsSorted3" ) {}
 void runTest() { suite_SortTest.testQuickIsSorted3(); }
} testDescription_suite_SortTest_testQuickIsSorted3;

static class TestDescription_suite_SortTest_testQuickIsSorted4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testQuickIsSorted4() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 351, "testQuickIsSorted4" ) {}
 void runTest() { suite_SortTest.testQuickIsSorted4(); }
} testDescription_suite_SortTest_testQuickIsSorted4;

static class TestDescription_suite_SortTest_testQuickIsSorted5 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testQuickIsSorted5() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 358, "testQuickIsSorted5" ) {}
 void runTest() { suite_SortTest.testQuickIsSorted5(); }
} testDescription_suite_SortTest_testQuickIsSorted5;

static class TestDescription_suite_SortTest_testQuickIsSorted6 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testQuickIsSorted6() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 365, "testQuickIsSorted6" ) {}
 void runTest() { suite_SortTest.testQuickIsSorted6(); }
} testDescription_suite_SortTest_testQuickIsSorted6;

static class TestDescription_suite_SortTest_testQuickIsSorted7 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testQuickIsSorted7() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 372, "testQuickIsSorted7" ) {}
 void runTest() { suite_SortTest.testQuickIsSorted7(); }
} testDescription_suite_SortTest_testQuickIsSorted7;

static class TestDescription_suite_SortTest_testQuickIsSorted8 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testQuickIsSorted8() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 379, "testQuickIsSorted8" ) {}
 void runTest() { suite_SortTest.testQuickIsSorted8(); }
} testDescription_suite_SortTest_testQuickIsSorted8;

static class TestDescription_suite_SortTest_testQuickIsSorted9 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testQuickIsSorted9() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 386, "testQuickIsSorted9" ) {}
 void runTest() { suite_SortTest.testQuickIsSorted9(); }
} testDescription_suite_SortTest_testQuickIsSorted9;

static class TestDescription_suite_SortTest_testHeapIsSortedEmpty : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testHeapIsSortedEmpty() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 394, "testHeapIsSortedEmpty" ) {}
 void runTest() { suite_SortTest.testHeapIsSortedEmpty(); }
} testDescription_suite_SortTest_testHeapIsSortedEmpty;

static class TestDescription_suite_SortTest_testHeapIsSorted1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testHeapIsSorted1() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 401, "testHeapIsSorted1" ) {}
 void runTest() { suite_SortTest.testHeapIsSorted1(); }
} testDescription_suite_SortTest_testHeapIsSorted1;

static class TestDescription_suite_SortTest_testHeapIsSorted2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testHeapIsSorted2() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 408, "testHeapIsSorted2" ) {}
 void runTest() { suite_SortTest.testHeapIsSorted2(); }
} testDescription_suite_SortTest_testHeapIsSorted2;

static class TestDescription_suite_SortTest_testHeapIsSorted3 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testHeapIsSorted3() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 415, "testHeapIsSorted3" ) {}
 void runTest() { suite_SortTest.testHeapIsSorted3(); }
} testDescription_suite_SortTest_testHeapIsSorted3;

static class TestDescription_suite_SortTest_testHeapIsSorted4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testHeapIsSorted4() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 422, "testHeapIsSorted4" ) {}
 void runTest() { suite_SortTest.testHeapIsSorted4(); }
} testDescription_suite_SortTest_testHeapIsSorted4;

static class TestDescription_suite_SortTest_testHeapIsSorted5 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testHeapIsSorted5() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 429, "testHeapIsSorted5" ) {}
 void runTest() { suite_SortTest.testHeapIsSorted5(); }
} testDescription_suite_SortTest_testHeapIsSorted5;

static class TestDescription_suite_SortTest_testHeapIsSorted6 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testHeapIsSorted6() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 436, "testHeapIsSorted6" ) {}
 void runTest() { suite_SortTest.testHeapIsSorted6(); }
} testDescription_suite_SortTest_testHeapIsSorted6;

static class TestDescription_suite_SortTest_testHeapIsSorted7 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testHeapIsSorted7() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 443, "testHeapIsSorted7" ) {}
 void runTest() { suite_SortTest.testHeapIsSorted7(); }
} testDescription_suite_SortTest_testHeapIsSorted7;

static class TestDescription_suite_SortTest_testHeapIsSorted8 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testHeapIsSorted8() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 450, "testHeapIsSorted8" ) {}
 void runTest() { suite_SortTest.testHeapIsSorted8(); }
} testDescription_suite_SortTest_testHeapIsSorted8;

static class TestDescription_suite_SortTest_testHeapIsSorted9 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testHeapIsSorted9() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 457, "testHeapIsSorted9" ) {}
 void runTest() { suite_SortTest.testHeapIsSorted9(); }
} testDescription_suite_SortTest_testHeapIsSorted9;

static class TestDescription_suite_SortTest_testNonCompareIsSortedEmpty : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testNonCompareIsSortedEmpty() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 465, "testNonCompareIsSortedEmpty" ) {}
 void runTest() { suite_SortTest.testNonCompareIsSortedEmpty(); }
} testDescription_suite_SortTest_testNonCompareIsSortedEmpty;

static class TestDescription_suite_SortTest_testNonCompareIsSorted1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testNonCompareIsSorted1() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 472, "testNonCompareIsSorted1" ) {}
 void runTest() { suite_SortTest.testNonCompareIsSorted1(); }
} testDescription_suite_SortTest_testNonCompareIsSorted1;

static class TestDescription_suite_SortTest_testNonCompareIsSorted2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testNonCompareIsSorted2() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 479, "testNonCompareIsSorted2" ) {}
 void runTest() { suite_SortTest.testNonCompareIsSorted2(); }
} testDescription_suite_SortTest_testNonCompareIsSorted2;

static class TestDescription_suite_SortTest_testNonCompareIsSorted3 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testNonCompareIsSorted3() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 486, "testNonCompareIsSorted3" ) {}
 void runTest() { suite_SortTest.testNonCompareIsSorted3(); }
} testDescription_suite_SortTest_testNonCompareIsSorted3;

static class TestDescription_suite_SortTest_testNonCompareIsSorted4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testNonCompareIsSorted4() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 493, "testNonCompareIsSorted4" ) {}
 void runTest() { suite_SortTest.testNonCompareIsSorted4(); }
} testDescription_suite_SortTest_testNonCompareIsSorted4;

static class TestDescription_suite_SortTest_testNonCompareIsSorted5 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testNonCompareIsSorted5() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 500, "testNonCompareIsSorted5" ) {}
 void runTest() { suite_SortTest.testNonCompareIsSorted5(); }
} testDescription_suite_SortTest_testNonCompareIsSorted5;

static class TestDescription_suite_SortTest_testNonCompareIsSorted6 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testNonCompareIsSorted6() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 507, "testNonCompareIsSorted6" ) {}
 void runTest() { suite_SortTest.testNonCompareIsSorted6(); }
} testDescription_suite_SortTest_testNonCompareIsSorted6;

static class TestDescription_suite_SortTest_testNonCompareIsSorted7 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testNonCompareIsSorted7() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 514, "testNonCompareIsSorted7" ) {}
 void runTest() { suite_SortTest.testNonCompareIsSorted7(); }
} testDescription_suite_SortTest_testNonCompareIsSorted7;

static class TestDescription_suite_SortTest_testNonCompareIsSorted8 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testNonCompareIsSorted8() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 521, "testNonCompareIsSorted8" ) {}
 void runTest() { suite_SortTest.testNonCompareIsSorted8(); }
} testDescription_suite_SortTest_testNonCompareIsSorted8;

static class TestDescription_suite_SortTest_testNonCompareIsSorted9 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SortTest_testNonCompareIsSorted9() : CxxTest::RealTestDescription( Tests_SortTest, suiteDescription_SortTest, 528, "testNonCompareIsSorted9" ) {}
 void runTest() { suite_SortTest.testNonCompareIsSorted9(); }
} testDescription_suite_SortTest_testNonCompareIsSorted9;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";
