#ifndef SORT_TEST_H
#define SORT_TEST_H

#include "../include/MySort.h"

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <cxxtest/TestSuite.h>

using namespace std;

class SortTest : public CxxTest::TestSuite {
public:
  void (*f)(vector<int>& list);

  void setUp(){
    // TODO:
  }

  void fillRandom(vector<int>& vect) {
    srand(time(0));
    int s = 30;
	  for(int i = 0; i < s; i++){
		  vect.push_back(rand() % 20);
	  }
  }

  bool isSorted(vector<int> data) {
    for (unsigned int i = 1; i < data.size(); i++) {
      if (data[i] < data[i - 1]) {
        return false;
      }
    }
    return true;
  }

  // Selection Sort
  void testSelectionIsSortedEmpty() {
    f = selectionSort;
    vector<int> a;
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testSelectionIsSorted1() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testSelectionIsSorted2() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testSelectionIsSorted3() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testSelectionIsSorted4() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testSelectionIsSorted5() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testSelectionIsSorted6() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testSelectionIsSorted7() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testSelectionIsSorted8() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testSelectionIsSorted9() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  // Bubble Sort
  void testBubbleIsSortedEmpty() {
    f = bubbleSort;
    vector<int> a;
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testBubbleIsSorted1() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testBubbleIsSorted2() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testBubbleIsSorted3() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testBubbleIsSorted4() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testBubbleIsSorted5() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testBubbleIsSorted6() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testBubbleIsSorted7() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testBubbleIsSorted8() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testBubbleIsSorted9() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  // Merge Sort
  void testMergeIsSortedEmpty() {
    f = mergeSort;
    vector<int> a;
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeIsSorted1() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeIsSorted2() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeIsSorted3() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeIsSorted4() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeIsSorted5() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeIsSorted6() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeIsSorted7() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeIsSorted8() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeIsSorted9() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  // Merge Sort with Threshold
  void testMergeThresholdIsSortedEmpty() {
    f = mergeSort2;
    vector<int> a;
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeThresholdIsSorted1() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeThresholdIsSorted2() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeThresholdIsSorted3() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeThresholdIsSorted4() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeThresholdIsSorted5() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeThresholdIsSorted6() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeThresholdIsSorted7() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeThresholdIsSorted8() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testMergeThresholdIsSorted9() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  // Quick Sort
  void testQuickIsSortedEmpty() {
    f = quickSort;
    vector<int> a;
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testQuickIsSorted1() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testQuickIsSorted2() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testQuickIsSorted3() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testQuickIsSorted4() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testQuickIsSorted5() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testQuickIsSorted6() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testQuickIsSorted7() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testQuickIsSorted8() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testQuickIsSorted9() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  // Heap Sort
  void testHeapIsSortedEmpty() {
    f = heapSort;
    vector<int> a;
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testHeapIsSorted1() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testHeapIsSorted2() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testHeapIsSorted3() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testHeapIsSorted4() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testHeapIsSorted5() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testHeapIsSorted6() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testHeapIsSorted7() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testHeapIsSorted8() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testHeapIsSorted9() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  // Non Comparative Sort
  void testNonCompareIsSortedEmpty() {
    f = nonCompareSort;
    vector<int> a;
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testNonCompareIsSorted1() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testNonCompareIsSorted2() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testNonCompareIsSorted3() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testNonCompareIsSorted4() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testNonCompareIsSorted5() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testNonCompareIsSorted6() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testNonCompareIsSorted7() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testNonCompareIsSorted8() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }

  void testNonCompareIsSorted9() {
    vector<int> a;
    fillRandom(a);
    (*f)(a);
    TS_ASSERT(isSorted(a));
  }
};

#endif
