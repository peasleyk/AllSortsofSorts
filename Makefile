CC=g++
CFLAGS=-c -Wall -ggdb -I.
LDFLAGS=
SOURCES=include/MySort.h src/sortsandbox.cpp
EXECUTABLE=build/sortsandbox
TESTS=test/MySort_test.h

OBJECTS=$(SOURCES:.cpp=.o)

FLAGS   = -Iinclude

all: $(SOURCES) $(EXECUTABLE)

# These next lines do a bit of magic found from http://stackoverflow.com/questions/2394609/makefile-header-dependencies
# Essentially it asks the compiler to read the .cpp files and generate the needed .h dependencies.
# This way if any .h file changes the correct .cpp files will be recompiled
depend: .depend

.depend: $(SOURCES)
	rm -f ./.depend
	$(CC) $(CFLAGS) -MM $^ >> ./.depend;

include .depend
# End .h file magic

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf *o $(EXECUTABLE)
	rm -f ./.depend
	rm -f build/testrunner*
	rm -f build/labtestrunner*
	rm -rf coverage
	rm -f test/testrunner
	rm -f build/speed

# CXX Testing

CXXLOCATION = cxxtest
CXXTESTGEN = $(CXXLOCATION)/bin/cxxtestgen

test: test/testrunner
	test/./testrunner


testrunner: test/testrunner.cpp $(OBJECTS)
	g++ -I. -ggdb -Wall -I$(CXXLOCATION)/ -o test/testrunner test/testrunner.cpp


testrunner.cpp: $(HEADERS) $(TESTSSOURCES) $(SOURCES) $(TESTS)
	$(CXXTESTGEN) --error-printer -o test/testrunner.cpp $(TESTS)

coverage: testrunner
	rm -rf coverage
	mkdir coverage
	kcov --include-pattern=HW coverage test/./testrunner
	google-chrome-stable coverage/testrunner/index.html &

speed: src/speed.cpp $(OBJECTS)
	$(CC) -ggdb -O3 src/speed.cpp -o build/speed

graph:
	gnuplot --persist result.plg
