AllSortsOfSorts
===============
Overview
---------------------------
This was a homework  assignment for my data structures class that I felt useful enough to put out. This project contains multiple sorts, which can be used by including "MySort.h" and "MaxHeap.h" in your project. These sorts are compared against each other using the same random list of 1 to 2000 numbers and timed. The results can be outputted on a graph for comparison.

This project compares the following sorts for speed:
+ selection sort
+ bubble sort
+ mergesort
+ merge sort with a threshold
+ quicksort
+ heapSort
+ bucket sort

The project also includes an implementation of a max-heap used for heap sort.
New sorts can easily be added and tested.

Building
============
All binaries are compiled into build/

To build the driver to plot results, run
```
make speed
```
To build a testing sandbox, run 
```
make all
```

To remove all compiled binaries run
```
make clean
```

Generating the graph
=====================

After building speed, pipe the output into a text file
```
cd build
./speed > ../result.csv
```
Then display the graph with gnuplot installed
```
make graph
```

![](doc/result.png)

Testing
============
To use test coverage, install cxxtest with your preferred package manager

Then, run
``` 
make test
```
Credits
=======

Brian Wood
Kyle Peasley

