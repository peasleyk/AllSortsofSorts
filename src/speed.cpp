/*
	Sort Speed
	Kyle PEasley
	December  2015
*/

#include <string>
#include <iostream>
#include <stdlib.h>
#include "unistd.h"
#include <algorithm>
#include <time.h>
#include <ctime>
#include <vector>

#include "../include/MySort.h"

using namespace std;

double do_bubble(vector<int> to_sort){
		clock_t start, stop;
		start = clock();
		bubbleSort(to_sort);
		stop = clock();
		double time_to_sort = (stop - start)/(double)(CLOCKS_PER_SEC / 1000);
		return time_to_sort;
}

double do_selection(vector<int> to_sort){
		clock_t start, stop;
		start = clock();
		selectionSort(to_sort);
		stop = clock();
		double time_to_sort = (stop - start)/(double)(CLOCKS_PER_SEC / 1000);
		return time_to_sort;
}

double do_merge(vector<int> to_sort){
		clock_t start, stop;
		start = clock();
		mergeSort(to_sort);
		stop = clock();
		double time_to_sort = (stop - start)/(double)(CLOCKS_PER_SEC / 1000);
		return time_to_sort;
}

double do_merge_threshhold(vector<int> to_sort){
		clock_t start, stop;
		start = clock();
		mergeSort2(to_sort);
		stop = clock();
		double time_to_sort = (stop - start)/(double)(CLOCKS_PER_SEC / 1000);
		return time_to_sort;
}

double do_quick(vector<int> to_sort){
		clock_t start, stop;
		start = clock();
		quickSort(to_sort);
		stop = clock();
		double time_to_sort = (stop - start)/(double)(CLOCKS_PER_SEC / 1000);
		return time_to_sort;
}

double do_heap(vector<int> to_sort){
		clock_t start, stop;
		start = clock();
		heapSort(to_sort);
		stop = clock();
		double time_to_sort = (stop - start)/(double)(CLOCKS_PER_SEC / 1000);
		return time_to_sort;
}

double do_noncompare(vector<int> to_sort){
		clock_t start, stop;
		start = clock();
		nonCompareSort(to_sort);
		stop = clock();
		double time_to_sort = (stop - start)/(double)(CLOCKS_PER_SEC / 1000);
		return time_to_sort;
}
double build_in_sort(vector<int> to_sort){
	clock_t start, stop;
		start = clock();
		sort(to_sort.begin(),to_sort.end());
		stop = clock();
		double time_to_sort = (stop - start)/(double)(CLOCKS_PER_SEC / 1000);
		return time_to_sort;
}

vector<int> get_vector(int size_of_vector){
		vector<int> ran_numbers;;
		for (int j = 0; j <size_of_vector ; j++) {
    	int r =  rand() %  700000; // careful here!
    	ran_numbers.push_back(r);
		}
		return ran_numbers;
}

// void clear_cache(){
//      const int size = 20*1024*1024; // Allocate 20M. Set much larger then L2
//      char *c = (char *)malloc(size);
//      for (int i = 0; i < 0xffff; i++)
//        for (int j = 0; j < size; j++)
//          c[j] = i*j;
//  }









int main(int argc, char* argv[]){
//	clear_cache();
	int step = 2;
	for( int i = 0; i <1000; i++){
		vector<int> ran_vector = get_vector(step);
		vector<int> c(ran_vector);
		cout << step  << " " << " " << do_heap(ran_vector)<< " "
		<<  do_noncompare(ran_vector) << " " << build_in_sort(ran_vector) <<  " "
		<< do_quick(ran_vector) << " " << do_bubble(ran_vector) << " " << do_selection(ran_vector) << " "
		<< do_merge(ran_vector) << " " << do_merge_threshhold(ran_vector) << endl;


		step+=2;
}
}
