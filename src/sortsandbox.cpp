/*
	Sort Sandbox
	Paul Talaga
	November 2015
*/

#include <iostream>
#include <stdlib.h>
#include "include/MySort.h"

using namespace std;

int main(int argc, char* argv[]){

  cout << endl;

  // Bubble Sort
  cout << "Starting Bubble Sort" << endl;
	srand(time(0));
  vector<int> a;

  int s1 = 30;
	for(int i = 0; i < s1; i++){
		a.push_back(rand() % 20);
	}
	for(int i = 0; i < s1; i++){
		cout << i << ":" << a[i] << "\t";
	}

	cout << endl << endl;

	bubbleSort(a);

	for(int i = 0; i < s1; i++){
		cout << i << ":" << a[i] << "\t";
	}
	cout << endl << "Bubble Sort Done" << endl;

	// Heap Sort
	cout << "Starting Heap Sort" << endl;
	srand(time(0));
  vector<int> b;
  int s2 = 30;
	for(int i = 0; i < s2; i++){
		b.push_back(rand() % 20);
	}
	for(int i = 0; i < s2; i++){
		cout << i << ":" << b[i] << "\t";
	}

	cout << endl << endl;

	heapSort(b);

	for(int i = 0; i < s2; i++){
		cout << i << ":" << b[i] << "\t";
	}
	cout << endl << "Heap Sort Done" << endl;

	// Merge Sort (No threshold)
	cout << "Starting Merge Sort (No Threshold)" << endl;
  srand(time(0));
  vector<int> c;
  int s3 = 30;
	for(int i = 0; i < s3; i++){
		c.push_back(rand() % 20);
	}
	for(int i = 0; i < s3; i++){
		cout << c[i] << "\t";
	}

	cout << endl << endl;

	mergeSort(c);

	for(int i = 0; i < s3; i++){
		cout << c[i] << "\t";
	}
	cout << endl << "Merge Sort (No Threshold) Done" << endl;

	// Quick Sort
	cout << "Starting Quick Sort" << endl;
  srand(time(0));
  vector<int> d;
  int s4 = 30;
	for(int i = 0; i < s4; i++){
		d.push_back(rand() % 20);
	}
	for(int i = 0; i < s4; i++){
		cout << d[i] << "\t";
	}

	cout << endl << endl;

	mergeSort(d);

	for(int i = 0; i < s4; i++){
		cout << d[i] << "\t";
	}
	cout << endl << "Quick Sort Done" << endl;

	// Heap Sort
	cout << "Starting Heap Sort" << endl;
  srand(time(0));
  vector<int> e;
  int s5 = 30;
	for(int i = 0; i < s5; i++){
		e.push_back(rand() % 20);
	}
	for(int i = 0; i < s5; i++){
		cout << e[i] << "\t";
	}

	cout << endl << endl;

	mergeSort(e);

	for(int i = 0; i < s5; i++){
		cout << e[i] << "\t";
	}
	cout << endl << "Heap Sort Done" << endl;

	// Selection Sort
	cout << "Starting Selection Sort" << endl;
  srand(time(0));
  vector<int> f;
  int s6 = 30;
	for(int i = 0; i < s6; i++){
		f.push_back(rand() % 20);
	}
	for(int i = 0; i < s6; i++){
		cout << f[i] << "\t";
	}

	cout << endl << endl;

	mergeSort(f);

	for(int i = 0; i < s6; i++){
		cout << f[i] << "\t";
	}
	cout << endl << "Selection Sort Done" << endl;

	// Non Comparative Sort
	cout << "Starting Non Comparative Sort" << endl;
  srand(time(0));
  vector<int> g;
  int s7 = 30;
	for(int i = 0; i < s7; i++){
		g.push_back(rand() % 20);
	}
	for(int i = 0; i < s7; i++){
		cout << g[i] << "\t";
	}

	cout << endl << endl;

	mergeSort(g);

	for(int i = 0; i < s7; i++){
		cout << g[i] << "\t";
	}
	cout << endl << "Non Comparative Sort Done" << endl;

	// Merge Sort (With threshold)
	cout << "Starting Merge Sort With Threshold" << endl;
  srand(time(0));
  vector<int> h;
  int s8 = 30;
	for(int i = 0; i < s8; i++){
		h.push_back(rand() % 20);
	}
	for(int i = 0; i < s8; i++){
		cout << h[i] << "\t";
	}

	cout << endl << endl;

	mergeSort(h);

	for(int i = 0; i < s8; i++){
		cout << h[i] << "\t";
	}
	cout << endl << "Merge Sort With Threshold Done" << endl;
}
